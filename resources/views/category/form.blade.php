@extends('home')
@section('main')
    <div class="col-10">
        <h3>Add new post</h3>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{route('admin.category.store')}}">
            @csrf
            <div>
                <label for="Title" class="form-label">Title</label>
                <input type="text" min="5" max="100" class="form-control" id="exampleInputEmail1"
                       aria-describedby="emailHelp"
                       name="title" value="{{old('title',$category->title??null)}}">
            </div>

            <div>
                <label for="Body" class="form-label">Slug</label>
                <textarea type="text" class="form-control"
                          name="slug" cols="10" value="{{old('slug',$category->slug??null)}}">{{old('slug',$category->slug??null)}}</textarea>
            </div>
            <div>
                <button type="submit" class="btn btn-primary btn-block">Save category</button>
            </div>
        </form>
    </div>
@endsection
