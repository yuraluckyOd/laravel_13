@extends('home')
@section('main')
    <div class="container">
        <div class="row">
            <h1>Categories</h1>
            <a href="{{route('admin.category.create')}}" class="btn btn-primary btn-block">Add new category</a>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Title</th>
                    <th scope="col">Slug</th>

                    <th scope="col">Date of updated</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <th scope="row">{{$category->title}}</th>

                        <td>{{$category->slug}}</td>
                        <td>{{$category->created_at->diffForHumans()}}</td>
                        <td><a href="{{route('admin.category.edit',$category)}}" class="btn btn-primary">Edit</a><a href="{{route('admin.category.destroy',$category)}}" class="btn btn-danger">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
