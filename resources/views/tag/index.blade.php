@extends('home')
@section('main')
    <div class="container">
        <div class="row">
            <h1>Tags</h1>
            <a href="{{route('admin.tag.create')}}" class="btn btn-primary btn-block">Add new tag</a>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Title</th>
                    <th scope="col">Slug</th>

                    <th scope="col">Date of updated</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tags as $tag)
                    <tr>
                        <th scope="row">{{$tag->title}}</th>

                        <td>{{$tag->slug}}</td>
                        <td>{{$tag->created_at->diffForHumans()}}</td>
                        <td><a href="{{route('admin.tag.edit',$tag)}}" class="btn btn-primary">Edit</a><a href="{{route('admin.tag.destroy',$tag)}}" class="btn btn-danger">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
