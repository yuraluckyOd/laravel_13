@extends('home')
@section('main')
    <div class="col-10">
        <h3>Add new post</h3>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{route('admin.post.store')}}">
            @csrf
            <div>
                <label for="Title" class="form-label">Title</label>
                <input type="text" min="5" max="100" class="form-control" id="exampleInputEmail1"
                       aria-describedby="emailHelp"
                       name="title" value="{{old('title',$post->title??null)}}">
            </div>
            <div>
                <label for="Name" class="form-label">Name</label>
                <select name="user_id" class="form-control">
                    @foreach($users as $user)
                        <option @if($user->id==old('user_id',$post->user_id??null)) selected
                                @endif value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label for="Category" class="form-label">Category</label>
                <select name="category_id" class="form-control">
                    @foreach($categories as $category)
                        <option @if($category->id==old('category_id',$post->category_id??null)) selected
                                @endif value="{{$category->id}}">{{$category->title}}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label for="Tags" class="form-label">Tags</label>
                <select name="tags[]" multiple class="form-control">
                    @foreach($tags as $tag)
                        <option @if(in_array($tag->id,old('tags',$post->tags->pluck('id')->toArray()) ?? [])) selected
                                @endif value="{{$tag->id}}">{{$tag->title}}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label for="Body" class="form-label">Body</label>
                <textarea type="text" class="form-control"
                          name="body" cols="10" value="{{old('body',$post->body??null)}}">{{old('body',$post->body??null)}}</textarea>
            </div>
            <div>
                <button type="submit" class="btn btn-primary btn-block">Save post</button>
            </div>
        </form>
    </div>
@endsection
