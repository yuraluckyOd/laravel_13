@extends('home')
@section('main')
    <div class="container">
        <div class="row">
            <h1>Posts</h1>
            <a href="{{route('admin.post.create')}}" class="btn btn-primary btn-block">Add new post</a>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Title</th>
                    <th scope="col">Author</th>
                    <th scope="col">Category</th>
                    <th scope="col">Tags</th>
                    <th scope="col">Text</th>
                    <th scope="col">Date of updated</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <th scope="row">{{$post->title}}</th>
                        <td>{{$post->user->name}}</td>
                        <td>{{$post->category->title}}</td>
                        <td>@foreach($post->tags as $tag)
                                <span>{{$tag->title}}</span>
                            @endforeach
                        </td>
                        <td>{{$post->body}}</td>
                        <td>{{$post->created_at->diffForHumans()}}</td>
                        <td><a href="{{route('admin.post.edit',$post)}}" class="btn btn-primary">Edit</a><a href="{{route('admin.post.destroy',$post)}}" class="btn btn-danger">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
