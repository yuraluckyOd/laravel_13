@extends('layout')
@section('content')
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link " aria-current="page" href="/">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('admin.post.index')}}">Posts</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('admin.tag.index')}}">Tags</a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="{{route('admin.category.index')}}" tabindex="-1"
               aria-disabled="true">Categories</a>
        </li>

    </ul>
    <ul class="nav nav-tabs offset-md-6 ">
        @auth
            <li class="nav-item justify-content-end">
                <a class="nav-link " href="{{route('auth.logout')}}" tabindex="-1" aria-disabled="true">Hello {{\Illuminate\Support\Facades\Auth::user()->name}}   Logout</a>
            </li>
        @endauth
        @guest
            <li class="nav-item justify-content-end">
                <a class="nav-link " href="{{route('auth.login')}}" tabindex="-1" aria-disabled="true">Login</a>
            </li>
        @endguest

    </ul>
    @yield('main')
@endsection
