<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login()
    {
        return view('login.login');
    }

    public function handleLogin(Request $request)
    {
        $data = $request->validate([
            'email' => ['required'],
            'password' => ['required', 'min:3']
        ]);

        if (Auth::attempt($data)) {
            if (Hash::needsRehash($data['password'])) {
                $user = Auth::user();
                $user->password = Hash::make($data['password']);
                $user->save();
            }
            return redirect()->route('home');
        }
        return back()->withErrors([
            'email' => 'Not matches.Try again',
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}
