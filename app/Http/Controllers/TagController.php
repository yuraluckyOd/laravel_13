<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     */
    public function index()
    {
        $tags = Tag::paginate(10);
        return view('tag.index', compact('tags'));
    }


    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $tag = new Tag ();
        return view('tag.form', compact('tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => ['required', 'min:5'],
            'slug' => ['required', 'min:5']
        ]);
        Tag::create($data);
        return redirect()->route('admin.tag.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Tag $tag
     * @return void
     */
    public function edit(Tag $tag)
    {
        return view('tag.form', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Tag $tag
     * @return Response
     */
    public function update(Request $request, Tag $tag)
    {
        $data = $request->validate([
            'title' => ['required', 'min:5'],
            'slug' => ['required', 'min:5']
        ]);
        $tag->update($data);
        return redirect()->route('admin.tag.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tag $tag
     * @return Response
     * @throws \Exception
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();
        return redirect()->route('admin.tag.index');
    }
}
