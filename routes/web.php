<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');


Route::middleware('guest')->group(function () {
    Route::get('auth/login', [AuthController::class, 'login'])->name('auth.login');
    Route::post('auth/login', [AuthController::class, 'handlelogin'])->name('auth.handlelogin');
});

Route::middleware('auth')->group(function (){
    Route::get('posts', [PostController::class, 'index'])->name('admin.post.index');
    Route::get('posts/create', [PostController::class, 'create'])->name('admin.post.create');
    Route::post('posts/create', [PostController::class, 'store'])->name('admin.post.store');
    Route::get('posts/{post}/edit', [PostController::class, 'edit'])->name('admin.post.edit');
    Route::post('posts/{post}', [PostController::class, 'update'])->name('admin.post.update');
    Route::get('posts/{post}/destroy', [PostController::class, 'destroy'])->name('admin.post.destroy');

    Route::get('categories', [CategoryController::class, 'index'])->name('admin.category.index');
    Route::get('categories/create', [CategoryController::class, 'create'])->name('admin.category.create');
    Route::post('categories/create', [CategoryController::class, 'store'])->name('admin.category.store');
    Route::get('categories/{category}/edit', [CategoryController::class, 'edit'])->name('admin.category.edit');
    Route::post('categories/{category}', [CategoryController::class, 'update'])->name('admin.category.update');
    Route::get('categories/{category}/destroy', [CategoryController::class, 'destroy'])->name('admin.category.destroy');

    Route::get('tags', [TagController::class, 'index'])->name('admin.tag.index');
    Route::get('tags/create', [TagController::class, 'create'])->name('admin.tag.create');
    Route::post('tags/create', [TagController::class, 'store'])->name('admin.tag.store');
    Route::get('tags/{tag}/edit', [TagController::class, 'edit'])->name('admin.tag.edit');
    Route::post('tags/{tag}', [TagController::class, 'update'])->name('admin.tag.update');
    Route::get('tags/{tag}/destroy', [TagController::class, 'destroy'])->name('admin.tag.destroy');

    Route::get('auth/logout', [AuthController::class, 'logout'])->name('auth.logout')->middleware('auth');

});




